#include <Arduino.h>
#include <ESP8266WiFi.h>

#ifndef ENA
#define ENA 1
#endif
#ifndef IN1
#define IN1 16
#endif
#ifndef IN2
#define IN2 5
#endif
#ifndef IN3
#define IN3 4
#endif
#ifndef IN4
#define IN4 14
#endif
#ifndef ENB
#define ENB 12
#endif
#ifndef ECHO
#define ECHO 13
#endif
#ifndef TRIGGER
#define TRIGGER 15
#endif

int port = 7218;
WiFiServer server(port);

const char *ssid = "rasJaveriana";
const char *passwd = "rasJaveriana2023";

double t;
double d;

int vel;

void movimiento(String read_ros);
void adelante();
void atras();
void izquierda();
void derecha();
void cambiar_vel(String read_ros);
void read_sensor();
void apagar_robot();


void setup() {
  Serial.begin(115200);
  pinMode(ENA, OUTPUT); 
  pinMode(IN1, OUTPUT); 
  pinMode(IN2, OUTPUT); 
  pinMode(IN3, OUTPUT); 
  pinMode(IN4, OUTPUT); 
  pinMode(ENB, OUTPUT); 
  pinMode(TRIGGER, OUTPUT); 
  pinMode(ECHO, INPUT); 
  digitalWrite(TRIGGER, LOW);
  analogWrite(ENA, LOW);
  analogWrite(ENB, LOW);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, passwd);
  while(WiFi.status() != WL_CONNECTED){
    Serial.println(".");
  }

  Serial.println("");
  Serial.print("Direccion IP local: ");
  Serial.println(WiFi.localIP());

  server.begin();
  vel = 0;
  d = 0.0;
  t = 0;
}

void loop() {
  WiFiClient client = server.available();
  String read_ros = "";
  read_sensor();

  if(client){
      if(client.connected()){
        Serial.print("Cliente conectado");
      }

      while(client.connected()){
        read_sensor();
        while(client.available() > 0) {
          char data = client.read();
          read_ros.concat(data);
        }

        if(read_ros.compareTo("") != 0) {
          Serial.println(read_ros);
        
          if(d > 5.00 || read_ros.indexOf("stop") != -1){
            apagar_robot();
          } else{
            movimiento(read_ros);
          }
            
          read_ros = "";
        }
      }
      client.stop();
      Serial.println("Cliente desconectado");
      apagar_robot();
  }
  apagar_robot();
}

void movimiento(String read_ros){
  if(read_ros.indexOf("adelante") != -1){
    digitalWrite(IN1, HIGH);
    digitalWrite(IN2, LOW);
    digitalWrite(IN3, HIGH);
    digitalWrite(IN4, LOW);
  }else if(read_ros.indexOf("atras") != -1){
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, HIGH);
    digitalWrite(IN3, LOW);
    digitalWrite(IN4, HIGH);
  }else if(read_ros.indexOf("izquierda") != -1){
    digitalWrite(IN1, HIGH);
    digitalWrite(IN2, LOW);
    digitalWrite(IN3, LOW);
    digitalWrite(IN4, HIGH);
  }else if(read_ros.indexOf("derecha") != -1){
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, HIGH);
    digitalWrite(IN3, HIGH);
    digitalWrite(IN4, LOW); 
  }else{
    vel = read_ros.toInt();
  }
  digitalWrite(ENA, vel);
  digitalWrite(ENB, vel);
}

void read_sensor() {
  digitalWrite(TRIGGER, HIGH);
  delayMicroseconds(10);          
  digitalWrite(TRIGGER, LOW);
  t = pulseIn(ECHO, HIGH); 
  d = t/59.0;
}

void apagar_robot() {
  digitalWrite(ENA, 0);
  digitalWrite(ENB, 0);
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, LOW);
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, LOW);
}